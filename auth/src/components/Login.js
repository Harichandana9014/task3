import React, { Component } from 'react'
import { login } from './UserFunctions'
import { withRouter } from 'react-router'

class Login extends Component {

    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
            error:''
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })

    }
    onSubmit(e) {
        e.preventDefault()

        const user = {
            email: this.state.email,
            password: this.state.password
        }
        login(user).then(res => {
            
           if(res.response.status===200)
           {
               this.setState({error:''})
               alert(" Welcome ",this.state.email)
                this.props.history.push('/update')     
           }           
           
        }).catch(error=>{
            this.setState({error:'Invalid details'})
            alert(" If alredy register check your mail for verification link ")
            console.log(error)
        })
       
    }
    render() {
        return (
        <div className="ui container" style={{position:'center'}}>
            <form noValidate onSubmit={this.onSubmit}>
                <h1 className="h3 mb-3 font-weight-normal">Sign In</h1>
                <span style={{color:'red'}}>{this.state.error}</span>
                <div className="from-group">                    
                    <input type="email"
                        name="email"
                        placeholder="Enter Email"
                        value={this.state.email}
                        onChange={this.onChange} />
                </div>
                <div className="from-group">
                    <input type="password"                
                        name="password"
                        placeholder="Enter Password"
                        value={this.state.password}
                        onChange={this.onChange} />
                </div>
                <button type="submit">
                    SignIn
                 </button>
            </form>
            </div>

        );
    }
}

export default Login;