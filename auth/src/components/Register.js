import React, { Component } from 'react'
import { register } from './UserFunctions'

class Register extends Component {
    constructor() {
        super()
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            error:'',
            phone :''
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })

    }
    onSubmit(e) {
        e.preventDefault()

        const newUser = {
            // first_name:this.state.first_name,
            // last_name:this.state.last_name,
            email: this.state.email,
            password: this.state.password,
            phone : this.state.phone

        }
        register(newUser).then(res => {
            //console.log(res.error.response)
            if(res.error){
                if(res.error.response.status === 409) {
                    this.setState({ error : " User already register "})
                }
            }
            else {
                this.setState({error:''})
                console.log(res)
                alert(" Verification link has been sent to registered mail ")
                this.props.history.push('/verifyOtp')

            }
             //this.props.history.push('/register')
        })
    }
    render() {
        return (
            <div className="ui container">
                <form noValidate onSubmit={this.onSubmit}>
                    <h1 className="h3 mb-3 font-weight-normal">Sign up</h1>
                    <span style={{color: 'red'}}>{this.state.error}</span>
                    <div >
                        <input type="email"
                            name="email"
                            placeholder="Enter Email"
                            value={this.state.email}
                            onChange={this.onChange} />
                    </div>
                    <div >
                        <input type="password"
                            name="password"
                            placeholder="Enter Password"
                            value={this.state.password}
                            onChange={this.onChange} />
                    </div>
                    <div >
                        <input type="number"
                            name="phone"
                            placeholder="Enter Number"
                            value={this.state.phone}
                            onChange={this.onChange} />
                    </div>

                    <button type="submit" className="btn  btn-primary ">
                        SignUp
                    </button>

                </form>
            </div>
        );
    }
}

export default Register