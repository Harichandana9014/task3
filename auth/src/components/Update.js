import React from 'react'
import {update} from './UserFunctions'
class Update extends React.Component{
    constructor()
    {
        super()
        this.state={
            fname:'',
            lname:'',
            contact:''
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
   onChange=e=>{
        this.setState({[e.target.name]:[e.target.value]})
   }
   onSubmit=e=>{
       e.preventDefault()
       const user={
           fname:this.state.fname,
           lname:this.state.lname,
           contact:this.state.contact,
           
       }
       update(user).then(res=>{
           if(res.status==200)
                alert("Successfully updated")
            else    
                alert("Failed to update")
       })
   }
    render(){
        return(
                <div className="ui container">
                        <form onSubmit={this.onSubmit}>
                            <div>
                                <   input type="text"
                                    placeholder="Enter First Name"
                                    name="fname"
                                    onChange={this.onChange }
                                />
                            </div>
                            <div>
                                <   input type="text"
                                    placeholder="Enter last Name"
                                    name="lname"
                                    onChange={this.onChange }
                                />
                            </div>
                            <div>
                                <   input type="number"
                                    placeholder="Enter Contact Number"
                                    name="contact"
                                    onChange={this.onChange }
                                />
                            </div>
                            <button type="submit">Submit</button>                            
                        </form>
                </div>
        );

    }
}
export default Update