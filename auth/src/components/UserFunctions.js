import axios from 'axios'

export const register = newUser => {
    return axios
        .post('/authenticate/signup', {
            email: newUser.email,
            password: newUser.password,
            phone:newUser.phone
        })
        .then(response => {
            console.log(response.status)
            return {response:response}
        }).catch(err=>{
           
            return {error : err}
        })
}
export const otp = newUser =>{
    return axios
    .post('/confirm_otp',{
        otp:newUser.otp
    })
    .then(response=>{
        // console.log("response from otp",response)
        return response
    }).catch(err=>{
        console.log("otp",err)
        return err
    })
}
export const login = user => {
    return axios
        .post('/authenticate/signin', {
            email: user.email,
            password: user.password

        })
        .then(response => {
            localStorage.setItem('usertoken', response.data.token)
            console.log("response is",response) 
            return {response:response}
        })
        .catch(err => {
             console.log(err)
            return {error:err}
        })
}
export const update = user =>{
    return axios
    .put('/profile/update',{
            first_name:user.fname,
            last_name:user.lname,
            contact_number:user.contact            
    },{headers:
        {
            Authorization: `Bearer ${localStorage.getItem('usertoken')}`
        }
    }).then(res=>{
        console.log(res)
        return res
    }).catch(err=>{
        console.log("error",err)
        return err
    })
}
export const DeleteUser =()=>{
    return axios
    .delete('/user',{
        headers:{
            Authorization: `Bearer ${localStorage.getItem('usertoken')}`
        }
    })
    .then(res=>{
        console.log("res",res)
        return res
    })
    .catch(err=>{
        console.log(err)
        return err
    })
}
export const profile = () => {
    return axios
    .post('/user/profile',{
        headers:{
            Authorization: `Bearer ${localStorage.getItem('usertoken')}`
        }
    }).then(res=>{
        console.log(res)
        return res
    }).catch(err=>{
        
        return err
    })
}

