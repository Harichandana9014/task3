import React,{Component} from 'react';
import {NavLink} from 'react-router-dom';


const NavBar=()=>{
    return(
        <div className="ui container"> 
            <NavLink style={{color:'black',margin:50}}to="/register">Register</NavLink>
            <NavLink style={{color:'black',margin:50}} to="/login">Login</NavLink>
        </div>
    );
}
 export default NavBar;