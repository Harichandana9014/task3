import React from 'react'
import {otp} from './UserFunctions'

class Otp extends React.Component{
    constructor(){
        super()
        this.state={
            otp:''
        }
        this.onSubmit = this.onSubmit.bind(this)
    }
    onSubmit(e){
            e.preventDefault()
            const newUser ={
                otp:this.state.otp
            } 
            otp(newUser).then(Response =>{
                console.log(Response)
                alert("OTP successfully verified")
                this.props.history.push('/login')
            }).catch(err =>{
                console.log(err)
            })
    }
    render(){
        return(
            <div className="ui container">
                <form onSubmit={this.onSubmit}>
                    <div>
                        <input type="number"
                        placeholder ="Enter otp"
                        name ="otp"
                        value={this.state.otp}
                        onChange={e=>this.setState({otp:e.target.value})}
                        />
                    </div>
                    <button type="submit" className="btn  btn-primary ">
                        Submit
                    </button>
                </form>
            </div>
        );
    }
}
export default Otp