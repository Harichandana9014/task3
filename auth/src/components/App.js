import React, { Component } from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import NavBar from './NavBar'
import Login from './Login'
import Register from './Register'
import Update from './Update'
import Delete from './Delete'
import Profile from './Profile'
import Page from './Page'
import Otp from './Otp'
class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
       <NavBar/> 
       <Switch>
         
        <Route exact path="/register" component={Register}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path='/update' component={Update}/>
        <Route exact path='/delete' component={Delete}/>
        <Route exact path='/profile' component={Profile}/>
        <Route exact path='/verifyOtp' component={Otp}/>
        <Route path='/page' component={Page}/>
        </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
