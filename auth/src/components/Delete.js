import React from 'react'
import {DeleteUser} from './UserFunctions'
class Delete extends React.Component{
    onSubmit=()=>{
        DeleteUser ().then(res=>{
            if(res.status===200)
            {
                alert("User Deleted",localStorage.getItem('usertoken'))

            }
            else{
                alert("User not deleted")
            }
        }).catch(err=>{
            console.log("error is",err)
        })
    }
    render()
    {
        return(
            <div>
                <button onClick={this.onSubmit}>Delete User</button>
            </div>
        );
    }
}
export default Delete