import os
from flask import Flask,jsonify,json,request,redirect, render_template, url_for,flash
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from flask_cors import CORS
from datetime import datetime
from flask_bcrypt import Bcrypt 
from flask_mail import Mail
from flask_mail import Message
import smtplib
from email.mime.text import MIMEText
import plivo
from random import randint
from itsdangerous import URLSafeTimedSerializer,SignatureExpired
from flask_jwt_extended import create_access_token
import jwt

import json
from json import *
app = Flask(__name__)


app.config['MONGO_DBNAME']  =   'mongotask'
app.config['MONGO_URI']     =   'mongodb://localhost:27017/mongotask'
app.config['JWT']           =   'secret'
authId                      =   'MAM2FJNDKWN2MWZDHJNZ'
authToken                   =   'OGI0MDYyZmU4Y2ExODZjNmFjNjhkMzNkMmRhMTgw'
p                           =   plivo.RestClient(authId,authToken)
app.config.from_pyfile('config.cfg')
mongo = PyMongo(app)
bcrypt = Bcrypt(app)
mail=Mail(app)

# jwt = JWTManager(app)


CORS(app)


# setting mail as global


def globalMail(m):
    global gmail
    gmail = m
    print ("gmail is", gmail)
# setting otp as global
def globalOtp(otp):
    global gotp
    gotp = otp
    print ("gotp",gotp)
    # user sign up contains registration ,token generation and email verification
@app.route('/authenticate/signup',methods=["POST"])
def register():
    users    = mongo.db.users
    email    = request.get_json()['email']
    phone    = request.get_json()['phone']
    hari     = "harichandana9014@gmail.com"
    password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')
    created  = datetime.utcnow()
    response = users.find_one({'email':email})
    if response:
        if response.get('confirmed'):
            result  = jsonify({'result' : "email already registerd"}),409
        result = jsonify ({'result': " Email is not verified "}),401
        
    else:
        user_id = users.insert({
            'email'     : email,
            'password'  : password,
            'phone'     : phone,
            'confirmed' : False,
            'status'    : False
        })
        otp = str(randint(1000,9999))
        msg =   p.messages.create(
            src = '919581483242',
            dst = '91'+phone,
            text = otp
        )
        globalMail(email)
        globalOtp(otp)
        print ("msg is" ,msg)
        token       = generate_confirmation_token(email)
        confirm_url = url_for('confirm_email', token=token, _external=True)
        subject     = "Please confirm your email"
        msg         = Message(subject,sender=hari,recipients=[email])
        msg.body    = "click the mail to confirm {}".format(confirm_url)
        mail.send(msg)
        
        result = jsonify({'email' : email +  '   registered'})
        
    return result

#  verification of otp
@app.route('/confirm_otp',methods=['POST'])
def confirm_otp():
        otp = request.get_json('otp')
        users = mongo.db.users
        otp2 = str(otp)
        res = gotp in otp2
        print ("otp is ",otp)
        print ("gopt i",gotp,"res",res)
        if (res):
            users.find_one_and_update({'email':gmail},{'$set':{'status':True}})
            result = "Verified"
        else :
            result = "Not Found"
        return jsonify(result)

# The link verification
@app.route('/confirm_mail/<token>')
def confirm_email(token):
    users   = mongo.db.users
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired.', 'danger')
    user = users.find_one_or_404({'email':email})
    print (user.get('confirmed'))   
    if user.get('confirmed'):
        flash('Account already confirmed. Please login.', 'success')
    else:
        users.update({"email":email},{'$set':{"confirmed":True}})
        user = users.find_one_or_404({'email':email})
        print (user.get('confirmed'))
        result  = "You have confirmed your account. Thanks!', 'success"
    return jsonify(result)

# send mail with subject and link
def send_email(to,subject,template,hari):
    msg = Message(subject,recipients = [to],html = template,sender = hari)
    mail.send(msg)


# token generated using secrete key  
def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    print (serializer)
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])


# returns the mail in the link
def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt    = app.config['SECURITY_PASSWORD_SALT'],
            max_age = expiration
        )
    except:
        return False
    return email
# after confirming the mail signup executed
@app.route('/authenticate/signin',methods=["POST"])
def login():
    users    = mongo.db.users
    email    = request.get_json()['email']
    password = request.get_json()['password']
    result   = ""

    response     = users.find_one({'email': email})
    access_token = ''
    
    if response:
        if response.get('status'):
            if response.get('confirmed'):
                if bcrypt.check_password_hash(response['password'],password):
                    access_token=jwt.encode({
                        'email' :response['email'].encode()
                    },app.config['JWT'])
                    if access_token:
                        result  = jsonify({'token' : access_token})
                    else:
                        result  = jsonify({'token not gererated '})
                else:
                    result = jsonify({'error' :'Invalid details','access ': access_token}),409
            else:
                result  = jsonify({'error' : 'Verification link has been sent to the registered mail '}),401
        else :
            result = jsonify({'error' : 'Please verify your mobile otp '}),401
    else:
        result= jsonify({'result' :'No results found' }),404
    
    return result

# token validation

def validateToken():
    token       = request.headers.get('Authorization')
    print ("token is",token)
    test,token  = token.split( )
    userdata    = jwt.decode(token,app.config['JWT'])
    print("email is",userdata)
    return userdata['email'].encode()

# profile updating
@app.route('/profile/update',methods=['PUT'])   
def update_task():
     email=validateToken()
     print(email)
     if email:
        users          = mongo.db.users
        first_name     = request.get_json()['first_name']
        last_name      = request.get_json()['last_name']
        contact_number = request.get_json()['contact_number']        
        response       = users.find_one_and_update({"email":email},{"$set":{"first_name":first_name},"$set":{"last_name":last_name},"$set":{"contact_number":contact_number}},upsert=False)
        
        if response :
            result  = {"result":"User profile Updated"}
        else:
            result  = {"result":"Not updated succesfully"}
        result      = {"result":"User profile Updated"}

        return jsonify({"result":result})
     return "Please login "


# displaying user profile 
@app.route('/user/profile',methods=['POST'])
def show_user():
    email   = validateToken()
    if email :
        users    = mongo.db.users
        result   = []
        response = (users.find_one({'email':email}))
        result1  = json.dumps(response,default=str)
        return (result1)

#  deleting user
@app.route('/user',methods=['DELETE'])
def delete_task() :
    email   = validateToken()
    if email:
        users    = mongo.db.users
        response = users.delete_one({'email':email})
        if response.deleted_count == 1:
            result = {'message':'record deleted'}
        else:
            result = {'message':'record not found'}
        return jsonify({'result':result})


# getting user list
@app.route('/userlist',methods=['GET'])
def get_all_tasks():
    users   = mongo.db.users
    result  = []
    for field in users.find():
        result.append({'_id':str(field['_id']),'email':field['email']})
    return jsonify(result)

    
# # @app.route('/api/task',methods=['POST'])
# # def add_task():
# #     tasks=mongo.db.tasks
# #     title=request.get_json()['title']
# #     task_id=tasks.insert({'title':title})
# #     new_task = tasks.find_one({'_id':task_id})
# #     result = {'title':new_task['title']}
# #     return jsonify({'result':result})


if __name__ == '__main__':
	app.run(debug=True) 